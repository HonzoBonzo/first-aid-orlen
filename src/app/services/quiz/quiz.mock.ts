export interface AnswerOption {
  id: string;
  value: string;
}

export interface Question {
  question: string;
  image: string;
  options: AnswerOption[];
  properAnswerId: string;
}

export interface Quiz {
  questions: Question[];
}

export const firstAidQuizMock: Quiz = {
  questions: [
    {
      question: "Główna zasada reanimacji to:",
      image:
        "https://samequizy.pl/wp-content/uploads/2016/02/filing_images_0a919c0c244e.jpeg",
      options: [
        {
          id: "q1a1",
          value: "40 ucisków, 2 wdechy"
        },
        {
          id: "q1a2",
          value: "30 ucisków, 2 wdechy"
        },
        {
          id: "q1a3",
          value: "40 ucisków, 4 wdechy"
        }
      ],
      properAnswerId: "q1a1"
    },
    {
      question: "Kto powinien udzielać pierwszej pomocy?",
      image:
        "https://samequizy.pl/wp-content/uploads/2016/02/filing_images_6803f7b341a5.jpeg",
      options: [
        {
          id: "q2a1",
          value: "każdy"
        },
        {
          id: "q2a2",
          value: "lekarz"
        },
        {
          id: "q2a3",
          value: "tylko ratownik medyczny"
        }
      ],
      properAnswerId: "q2a1"
    },
    {
      question: "Numer alarmowy to:",
      image:
        "https://samequizy.pl/wp-content/uploads/2016/02/filing_images_d8c91ac9b219.jpeg",
      options: [
        {
          id: "q3a1",
          value: "112"
        },
        {
          id: "q3a2",
          value: "122"
        },
        {
          id: "q3a3",
          value: "211"
        }
      ],
      properAnswerId: "q3a1"
    },
    {
      question:
        "Aby udrożnić drogi oddechowe chorego, podczas reanimacji należy:",
      image:
        "https://samequizy.pl/wp-content/uploads/2016/02/filing_images_0a919c0c244e.jpeg",
      options: [
        {
          id: "q4a1",
          value:
            "położyć go na brzuchu lub boku i dogiąć brodę do klatki piersiowej"
        },
        {
          id: "q4a2",
          value: "położyć go na plecach i odchylić głowę go tyłu"
        },
        {
          id: "q4a3",
          value:
            "tak go ułożyć, by głowa była zdecydowanie wyżej niż reszta ciała"
        }
      ],
      properAnswerId: "q4a1"
    },
    {
      question:
        "Aby udrożnić drogi oddechowe chorego, podczas reanimacji należy:",
      image:
        "https://samequizy.pl/wp-content/uploads/2016/02/filing_images_4df0e78831f0.jpeg",
      options: [
        {
          id: "q5a1",
          value:
            "położyć go na brzuchu lub boku i dogiąć brodę do klatki piersiowej"
        },
        {
          id: "q5a2",
          value: "położyć go na plecach i odchylić głowę go tyłu"
        },
        {
          id: "q5a3",
          value:
            "tak go ułożyć, by głowa była zdecydowanie wyżej niż reszta ciała"
        }
      ],
      properAnswerId: "q5a3"
    },
    {
      question: "Defiblyrator powinien byc użyty, gdy:",
      image:
        "https://samequizy.pl/wp-content/uploads/2016/02/filing_images_c4738f62a264.jpeg",
      options: [
        {
          id: "q6a1",
          value: "doszło do zaburzenia akcji serca"
        },
        {
          id: "q6a2",
          value: "jest podejrzenie urazu kręgosłupa u poszkodowanego"
        },
        {
          id: "q6a3",
          value: "poszkodowany został poparzony"
        }
      ],
      properAnswerId: "q6a1"
    },
    {
      question:
        "Czym charakteryzuje się poprawne wykonanie oddechu ratowniczego?:",
      image:
        "https://practicalevents.pl/wp-content/uploads/2019/01/pierwsza-pomoc.jpg",
      options: [
        {
          id: "q7a1",
          value: "Poszkodowany nagle gwałtownie łyka powietrze"
        },
        {
          id: "q7a2",
          value: "Klatka piersiowa poszkodowanego się unosi"
        },
        {
          id: "q7a3",
          value: "Poszkodowany zaczyna się krztusić"
        }
      ],
      properAnswerId: "q7a2"
    },
    {
      question: "W jakim rytmie wykonujemy ucisk klatki piersiowej?",
      image:
        "https://practicalevents.pl/wp-content/uploads/2019/01/pierwsza-pomoc.jpg",
      options: [
        {
          id: "q8a1",
          value: "Około 40 razy na minutę"
        },
        {
          id: "q8a2",
          value: "Około 60 razy na minutę"
        },
        {
          id: "q8a3",
          value: "Około 100 razy na minutę"
        }
      ],
      properAnswerId: "q8a1"
    }
  ]
};
