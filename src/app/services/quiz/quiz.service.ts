import { Injectable } from "@angular/core";
import { firstAidQuizMock, Question, AnswerOption } from "./quiz.mock";

@Injectable({
  providedIn: "root"
})
export class QuizService {
  quiz = firstAidQuizMock;

  constructor() {}

  getQuestion(): Question {
    const randomIndex = Math.floor(Math.random() * this.quiz.questions.length);

    return this.quiz.questions[randomIndex];
  }

  isAnswerCorrect(question: Question, answer: AnswerOption): boolean {
    return question.properAnswerId === answer.id;
  }
}
