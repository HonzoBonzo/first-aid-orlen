import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { OsmPlace, PlaceCoordinates } from './osm.model';
import { Observable } from 'rxjs/internal/Observable';
import { Subject } from 'rxjs';
import { distinctUntilChanged, switchMap } from 'rxjs/operators';

@Injectable()
export class OsmService {
  private engineUrl = 'https://nominatim.openstreetmap.org';
  reqGetPlaceByCoords = new Subject<PlaceCoordinates>();
  resGetPlaceByCoords = new Subject<OsmPlace>();

  constructor(private httpClient: HttpClient) {
    this.listenToGetPlaceByCoordsRequests();
  }

  listenToGetPlaceByCoordsRequests(): void {
    this.reqGetPlaceByCoords
      .pipe(
        distinctUntilChanged(),
        switchMap(placeCoords => this.getPlaceByCoords(placeCoords.lat, placeCoords.lng)),
      )
      .subscribe(osmPlace => this.resGetPlaceByCoords.next(osmPlace));
  }

  getPlaces(term: string): Observable<OsmPlace[]> {
    let httpParams = new HttpParams();
    httpParams = httpParams.set('format', 'json');
    httpParams = httpParams.set('addressdetails', '1');
    // httpParams = httpParams.set('country', 'polska');
    httpParams = httpParams.set('countrycodes', 'pl');
    httpParams = httpParams.set('q', `${term}`);
    httpParams = httpParams.set('limit', '7');

    return this.httpClient.get<OsmPlace[]>(this.engineUrl, { params: httpParams });
  }

  getPlaceByCoords(lat: number | string, lng: number | string): Observable<OsmPlace> {
    return this.httpClient.get<OsmPlace>(
      `${this.engineUrl}/reverse?format=jsonv2&lat=${lat}&lon=${lng}`,
    );
  }

}
