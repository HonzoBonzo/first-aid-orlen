import { icon, Marker, marker } from 'leaflet';

export const createMarker = (lat: number, lng: number): Marker =>
  marker([lat, lng], {
    icon: icon({
      iconSize: [25, 41],
      iconAnchor: [13, 41],
      iconUrl: 'assets/marker-icon.png',
      shadowUrl: 'assets/marker-shadow.png',
    }),
  });

export const createMarkerWithPopup = (lat: number, lng: number, text: string): Marker =>
  marker([lat, lng], {
    icon: icon({
      iconSize: [25, 41],
      iconAnchor: [13, 41],
      iconUrl: 'assets/leaflet/marker-icon.svg',
      shadowUrl: 'leaflet/marker-shadow.png'
    }),
  }).bindPopup(text);
