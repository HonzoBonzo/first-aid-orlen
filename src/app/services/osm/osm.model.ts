import { LeafletEvent } from "leaflet";
import * as _ from "lodash";

export class PlaceCoordinates {
  constructor(public lat: number, public lng: number) {}
}

export interface Address {
  city?: string;
  county?: string;
  state?: string;
  country?: string;
  country_code?: string;
  hamlet?: string;
  postcode?: string;
  road?: string;
  neighbourhood?: string;
}

export interface OsmPlace {
  lat: string;
  lon: string;
  place_id?: string;
  licence?: string;
  osm_type?: string;
  osm_id?: string;
  boundingbox?: string[];
  display_name?: string;
  class?: string;
  type?: string;
  importance?: number;
  icon?: string;
  place_rank?: string;
  category?: string;
  addresstype?: string;
  name?: string;
  address?: Address;
}

export class ApiPlaceModel {
  constructor(
    public lat: number,
    public lng: number,
    public road?: string,
    public neighbourhood?: string,
    public city?: string,
    public country?: string,
    public state?: string,
    public postcode?: string,
    public countryCode?: string,
    public name?: string,
    public displayName?: string
  ) {}

  static osmPlaceToApiPlace(osmPlace: OsmPlace): ApiPlaceModel {
    const address = _.get(osmPlace, "address", {}) as Address;

    return new ApiPlaceModel(
      parseFloat(osmPlace.lat),
      parseFloat(osmPlace.lon),
      address.road,
      address.neighbourhood,
      address.city,
      address.country,
      address.state,
      address.postcode,
      address.country_code,
      osmPlace.name,
      osmPlace.display_name
    );
  }

  static apiPlaceModelToOsmModel(apiPlaceModel: ApiPlaceModel): OsmPlace {
    return {
      lat: "" + apiPlaceModel.lat,
      lon: "" + apiPlaceModel.lng,
      address: {
        road: apiPlaceModel.road,
        neighbourhood: apiPlaceModel.neighbourhood,
        city: apiPlaceModel.city,
        country: apiPlaceModel.country,
        state: apiPlaceModel.state,
        postcode: apiPlaceModel.postcode,
        country_code: apiPlaceModel.countryCode
      },
      name: apiPlaceModel.name,
      display_name: apiPlaceModel.displayName
    };
  }
}

export type MyLeafletEvent = LeafletEvent & {
  latlng: { lat: number; lng: number };
};

export interface ApiLocation {
  address: string;
  description: string;
  id: number;
  lat: number;
  lng: number;
  location_description?: string;
  name?: string;
  phone?: string;
  type?: string
}
