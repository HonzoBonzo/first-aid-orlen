import { icon, latLng, MapOptions, marker, tileLayer } from 'leaflet';
import { PlaceCoordinates } from './osm.model';

export const krakowCoords: PlaceCoordinates = {
  lat: 50.0614792,
  lng: 19.93716649999999,
};

const leafletAttribution =
  '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>';
const wms = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';

export const krakowMarker = marker([krakowCoords.lat, krakowCoords.lng], {
  icon: icon({
    iconSize: [25, 41],
    iconAnchor: [13, 41],
    iconUrl: 'assets/marker-icon.png',
    shadowUrl: 'assets/marker-shadow.png',
  }),
});

export const defaultMapOptions: MapOptions = {
  layers: [
    tileLayer(wms, {
      maxZoom: 19,
      minZoom: 6,
      attribution: leafletAttribution,
    }),
  ],
  zoom: 15,
  center: latLng(krakowCoords.lat, krakowCoords.lng),
  doubleClickZoom: true,
  zoomControl: false,
};

export const initialLayersWithTileLayer = [
  tileLayer(wms, {
    maxZoom: 19,
    minZoom: 6,
    attribution: leafletAttribution,
  }),
];
