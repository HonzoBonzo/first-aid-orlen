import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrlenBarComponent } from './orlen-bar.component';

describe('OrlenBarComponent', () => {
  let component: OrlenBarComponent;
  let fixture: ComponentFixture<OrlenBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrlenBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrlenBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
