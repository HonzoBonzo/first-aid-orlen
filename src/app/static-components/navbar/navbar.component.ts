import { Component, OnInit } from "@angular/core";

import {
  faFacebookSquare,
  faTwitterSquare,
  faYoutubeSquare,
  faInstagram
} from "@fortawesome/free-brands-svg-icons";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { Router } from '@angular/router';

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"]
})
export class NavbarComponent implements OnInit {
  faFacebook = faFacebookSquare;
  faTwitter = faTwitterSquare;
  faYoutube = faYoutubeSquare;
  faInstagram = faInstagram;
  faSearch = faSearch;

  constructor(private router: Router) {}

  ngOnInit() {}
}
