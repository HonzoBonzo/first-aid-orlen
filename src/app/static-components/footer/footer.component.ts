import { Component, OnInit } from '@angular/core';

import {
  faFacebookSquare,
  faTwitterSquare,
  faYoutubeSquare,
  faInstagram
} from "@fortawesome/free-brands-svg-icons";
import { faSearch } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  faFacebook = faFacebookSquare;
  faTwitter = faTwitterSquare;
  faYoutube = faYoutubeSquare;
  faInstagram = faInstagram;
  faSearch = faSearch;

  constructor() { }

  ngOnInit() {
  }

}
