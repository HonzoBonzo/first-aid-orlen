import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { FooterComponent } from "./static-components/footer/footer.component";
import { NavbarComponent } from "./static-components/navbar/navbar.component";
import { OrlenBarComponent } from "./shared/orlen-bar/orlen-bar.component";
import { HomeViewComponent } from "./views/home-view/home-view.component";
import { FirstAidViewComponent } from "./views/first-aid-view/first-aid-view.component";
import { QuizViewComponent } from "./views/quiz-view/quiz-view.component";
import { MapSearchViewComponent } from "./views/map-search-view/map-search-view.component";
import { AccidentViewComponent } from "./views/accident-view/accident-view.component";
import { InsuranceViewComponent } from "./views/insurance-view/insurance-view.component";
import { FaqViewComponent } from "./views/faq-view/faq-view.component";
import { ChecklistBeforeTripViewComponent } from "./views/checklist-before-trip-view/checklist-before-trip-view.component";
import { EmergencyNumbersViewComponent } from "./views/emergency-numbers-view/emergency-numbers-view.component";
import { LeafletModule } from "@asymmetrik/ngx-leaflet";
import { OsmService } from './services/osm/osm.service';
import { QuizService } from './services/quiz/quiz.service';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    NavbarComponent,
    OrlenBarComponent,
    HomeViewComponent,
    FirstAidViewComponent,
    QuizViewComponent,
    MapSearchViewComponent,
    AccidentViewComponent,
    InsuranceViewComponent,
    FaqViewComponent,
    ChecklistBeforeTripViewComponent,
    EmergencyNumbersViewComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    FontAwesomeModule,
    LeafletModule.forRoot(),
    AppRoutingModule
  ],
  providers: [OsmService, QuizService],
  bootstrap: [AppComponent]
})
export class AppModule {}
