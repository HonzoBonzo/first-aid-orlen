import { Component, OnInit } from "@angular/core";
import { Question, AnswerOption } from "src/app/services/quiz/quiz.mock";
import { QuizService } from "src/app/services/quiz/quiz.service";
import { faCircle, faBookOpen } from "@fortawesome/free-solid-svg-icons";
import { delay, tap } from "rxjs/operators";
import { of } from "rxjs";

@Component({
  selector: "app-quiz-view",
  templateUrl: "./quiz-view.component.html",
  styleUrls: ["./quiz-view.component.scss"]
})
export class QuizViewComponent implements OnInit {
  question: Question;
  showOk = false;
  showBad = false;
  showAnswer = false;
  faOk = faCircle;
  faBad = faCircle;
  answered = false;

  constructor(private quizService: QuizService) {}

  ngOnInit() {
    this.fetchQuestion();
  }

  isOk(answer: AnswerOption): boolean {
    return this.quizService.isAnswerCorrect(this.question, answer);
  }

  answer(answer: AnswerOption): void {
    this.answered = true;
    if (this.quizService.isAnswerCorrect(this.question, answer)) {
      this.showOk = true;

      of(true)
        .pipe(
          delay(3000),
          tap(() => {
            this.resetQuestion();
          })
        )
        .subscribe();

      return;
    }

    this.showBad = true;
    this.showAnswer = true;
  }

  resetQuestion(): void {
    this.showBad = false;
    this.showOk = false;
    this.showAnswer = false;
    this.answered = false;
    this.fetchQuestion();
  }

  fetchQuestion(): void {
    this.question = this.quizService.getQuestion();
  }
}
