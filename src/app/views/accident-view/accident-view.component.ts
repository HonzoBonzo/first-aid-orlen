import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-accident-view",
  templateUrl: "./accident-view.component.html",
  styleUrls: ["./accident-view.component.scss"]
})
export class AccidentViewComponent implements OnInit {
  sections = [
    {
      title: "Jak poprawnie zabezpieczyć miejsce zdarzenia",
      name: "Zabezpiecz miejsce zdarzenia",
      image: "http://ferfecki.top/images/help-01.jpg",
      description:
        "Z tego poradnika dowiesz się jak poprawnie zabezpieczyć miejsce zdarzenia"
    },
    {
      title: "Krok 1",
      name: "Pamiętaj aby zadbać o swoje bezpieczeństwo",
      image: "http://ferfecki.top/images/help-02.jpg",
      description:
        "Nie będziesz mógł pomóc innym, jeśli sam zostaniesz poszkodowanym."
    },
    {
      title: "Krok 2",
      name: "Zatrzymaj bezpiecznie pojazd i zaciagnij hamulec ręczny",
      image: "http://ferfecki.top/images/help-03.jpg",
      description:
        "Zatrzymując się upewnij się, że zasłaniasz nim ofiarę wypadku i masz koła skręcone w zdala od ofiary."
    },
    {
      title: "Krok 3",
      name: "Załóż kamizekę odblaskową",
      image: "http://ferfecki.top/images/help-04.jpg",
      description:
        "Upewnij się że posiadasz na sobie kamizelkę odblaskową, lub inny odblaskowy ubiór"
    },
    {
      title: "Krok 4",
      name: "Włącz światła awaryjne",
      image: "http://ferfecki.top/images/help-05.jpg",
      description:
        "Jeżeli jest to możliwe zrób to później w innych samochodach bioracych udział w wypadku"
    },
    {
      title: "Krok 5",
      name: "Ustaw trójkąt ostrzegawczy 30-50 metrów od samochodu",
      image: "http://ferfecki.top/images/help-06.jpg",
      description:
        "Jeżeli jest to możliwe użyj trójkątów z innych samochodów bioracych udział w wypadku"
    },
    {
      title: "Krok 6",
      name: "Oceń liczbę rannych i ich obrażenia",
      image: "http://ferfecki.top/images/help-07.jpg",
      description:
        "Ostrożnie podejdź do aut biorących udział w kolizji i zweryfikuj"
    },
    {
      title: "Krok 7",
      name: "Wezwij służby ratownicze - 112",
      image: "http://ferfecki.top/images/help-08.jpg",
      description:
        "Twój komunikat winien zawierać następujące informacje: dane zgłaszającego i numer telefonu kontaktowego, miejsce zdarzenia – możliwa trasa dojazdu, liczba rannych i obrażenia, potrzeby dot. środków technicznych,"
    },
    {
      title: "Krok 8",
      name: "Unieruchom pojazdy w wypadku",
      image: "http://ferfecki.top/images/help-06.jpg",
      description: "Zaciągnij wyłącz silniki, hamulec ręczny"
    },
    {
      title: "Krok 9",
      name: "Udziel pierwszej pomocy poszkodowanym",
      image: "http://ferfecki.top/images/help-09.jpg",
      description:
        "Zakładaj zawsze możliwość przerwania rdzenia kręgowego. W razie zagrożenia pożarem wyciagnij ich ze strefy zagrożenia"
    },
    {
      title: "Krok 10",
      name: "Utrzymuj kontakt",
      image: "http://ferfecki.top/images/help-10.jpg",
      description:
        "Do czasu przybycia służ ratunkowych utrzymuj kontakt z ofiarami wypadku"
    }
  ];

  constructor() {}

  ngOnInit() {}
}
