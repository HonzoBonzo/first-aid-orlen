import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChecklistBeforeTripViewComponent } from './checklist-before-trip-view.component';

describe('ChecklistBeforeTripViewComponent', () => {
  let component: ChecklistBeforeTripViewComponent;
  let fixture: ComponentFixture<ChecklistBeforeTripViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChecklistBeforeTripViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChecklistBeforeTripViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
