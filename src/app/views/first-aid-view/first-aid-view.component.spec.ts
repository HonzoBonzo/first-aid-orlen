import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstAidViewComponent } from './first-aid-view.component';

describe('FirstAidViewComponent', () => {
  let component: FirstAidViewComponent;
  let fixture: ComponentFixture<FirstAidViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstAidViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstAidViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
