import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-first-aid-view",
  templateUrl: "./first-aid-view.component.html",
  styleUrls: ["./first-aid-view.component.scss"]
})
export class FirstAidViewComponent implements OnInit {
  adultSteps = [
    {
      "title": "Krok 0",
      "name": "Resusytacja dla noworodków/dzieci/dorosłych",
      "image": "http://ferfecki.top/images/aid_01.jpg",
      "description": "Resusytacja dla każdej z wymienionych grup różni się jedynie kilkoma drobiazgami"
    },
    {
      "title": "Krok 1",
      "name": "Sprawdz czy reaguje",
      "image": "http://ferfecki.top/images/aid_02.jpg",
      "description": "Potrząśnij i zobacz czy istnieje jakaś reakcja na bodźce zewnętrze, w przypadku ich braku zareaguj"
    },
    {
      "title": "Krok 2",
      "name": "Wskaż konkretną osobę do zadzownienia na 112",
      "image": "http://ferfecki.top/images/aid_02a.jpg",
      "description": "Jeżeli nie ma nikogo innego, zadzwoń sam"
    },
    {
      "title": "Krok 3",
      "name": "Udrożnij drogi oddechowe",
      "image": "http://ferfecki.top/images/aid_03.jpg",
      "description": "Sprawdź czy osoba nie ma niczego w ustach, jeżeli ma usuń obiekt"
    },
    {
      "title": "Krok 4",
      "name": "Odchyl głowę do tyłu",
      "image": "http://ferfecki.top/images/aid_04.jpg",
      "description": "W przypadku nowordków trzeba doatkowo przytrzymać szczękę do otwrcia ust. Jeżeli ciągle nie oddycha prawidłowo, przejdź do kolejnego kroku."
    },
    {
      "title": "Krok 5",
      "name": "Wykonaj 5 oddechów ratunkowych",
      "image": "http://ferfecki.top/images/aid_05.jpg",
      "description": "Każdy wdech powinien trwać jedną sekundę. Klatka pierwiowa powinna się wyraźnie usnieść podczas każdego z nich."
    },
    {
      "title": "Krok 6",
      "name": "Sprawdz czy reaguje",
      "image": "http://ferfecki.top/images/aid_02.jpg",
      "description": "Sprawdź czy osoba reaguje na bodźce, jeżeli nie to kontynuuj"
    },
    {
      "title": "Krok 7",
      "name": "30 ucisnięć klatki piersiowej",
      "image": "http://ferfecki.top/images/aid_07.jpg",
      "description": "W przypadku noworodków uzyj dwóch palców, w przypadku dzieci jednej ręki, w przypadku dorosłych dwóch rąk"
    },
    {
      "title": "Krok 8",
      "name": "2 oddechy ratownicze",
      "image": "http://ferfecki.top/images/aid_01.jpg",
      "description": ""
    },
    {
      "title": "Krok 9",
      "name": "Kontynuuj do momentu przyjechania służb ratunkowych",
      "image": "http://ferfecki.top/images/help-01.jpg",
      "description": "W razie czego poproś o pomoc"
    }
  ]

  childrenSteps = [
    {
      "title": "Resuscytacja dzieci",
      "name": "Nie reaguje?",
      "image": "",
      "description": "Wykonywać w przypadku"
    },
    {
      "title": "Resuscytacja dzieci",
      "name": "Wezwij pomoc",
      "image": "",
      "description": "Wskaż konkretną osobę która zadzwoni na 112. Jeżeli nie ma nikogo sam zadzwoń"
    },
    {
      "title": "Przygotowanie do resuscytacji",
      "name": "Udrożnij drogi oddechowe",
      "image": "",
      "description": "Sprawdź czy dziecko nie posiada niczego w ustach. Usuń obcy obiekt"
    },
    {
      "title": "Przygotowanie do resuscytacji",
      "name": "Nie oddycha prawidłowo",
      "image": "",
      "description": "Jeżeli ciągle nie oddycha prawidłowo, przejdź do kolejnego kroku"
    },
    {
      "title": "Oddechy ratowniczye",
      "name": "5 oddechów ratowniczych",
      "image": "https://2.bp.blogspot.com/-NX4uSchDVEc/W4ZdTns9OTI/AAAAAAAACbs/qsXni2eOZIwXsWvuF5Fgu3z_C6rt_CSIACLcBGAs/s320/dziecko%2Boddech.jpg",
      "description": "Odchyl głowę do tyłu trzymając za nasadę nosa i wykonaj 5 oddechów ratowniczych, przy każdym z oddechów klatka piersiowa powinna się ponieść. Każdy z oddechów powinnien trwać około 1 sekundy"
    },
    {
      "title": "Brak oznak zycia?",
      "name": "Przygotuj się do wykonywania ucisnięc klatki piersiowej",
      "image": "https://2.bp.blogspot.com/-kUI991D4JqY/W4Zd7wbjK8I/AAAAAAAACb8/IGbpDs7U0ZUYJZxB3YwtnoeHNFxmVV9MQCLcBGAs/s320/dziecko1.jpg",
      "description": "Używaj do tego jednej dłoni na wysokości mostka"
    },
    {
      "title": "Kontynuuj resuscytacje",
      "name": "15 ucisnieć klatki piersiowej, 2 oddechy ratownicze",
      "image": "https://2.bp.blogspot.com/-kUI991D4JqY/W4Zd7wbjK8I/AAAAAAAACb8/IGbpDs7U0ZUYJZxB3YwtnoeHNFxmVV9MQCLcBGAs/s320/dziecko1.jpg",
      "description": "Kontynuuować do odzyskania oddechu, lub przyjazdu pomocy"
    }
  ]

  constructor() {}

  ngOnInit() {}
}
