import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-emergency-numbers-view",
  templateUrl: "./emergency-numbers-view.component.html",
  styleUrls: ["./emergency-numbers-view.component.scss"]
})
export class EmergencyNumbersViewComponent implements OnInit {
  emergencygNumbers = [
    {
      imgSrc: "/assets/ikonki/112.png",
      name:
        "Centrum Powiadamiania Ratunkowego – ogólnoeuropejski numer alarmowy",
      phoneNr: "112"
    },
    {
      imgSrc: "/assets/ikonki/default.png",
      name: "Pogotowie Ratunkowe",
      phoneNr: "999"
    },
    {
      imgSrc: "/assets/ikonki/default.png",
      name: "Państwowa Straż Pożarna",
      phoneNr: "998"
    },
    {
      imgSrc: "/assets/ikonki/997.png",
      name:
        "Policja -dzwoniąc na ten numer, połączenie zostanie automatycznie przekierowane na numer 112",
      phoneNr: "997"
    },
    {
      imgSrc: "/assets/ikonki/999.png",
      name: "Pogotowie Ratunkowe",
      phoneNr: "999"
    },
    {
      phoneNr: "112",
      imgSrc: "/assets/ikonki/default.png",
      name: "Ogólnoeuropejski numer alarmowy"
    },
    {
      imgSrc: "/assets/ikonki/default.png",
      name: "Państwowa Straż Pożarna",
      phoneNr: "998"
    },
    {
      imgSrc: "/assets/ikonki/997.png",
      name: "Policja",
      phoneNr: "997"
    },
    {
      imgSrc: "/assets/ikonki/default.png",
      name: "Centrum Antyterrorystyczne",
      phoneNr: "996"
    },
    {
      imgSrc: "/assets/ikonki/default.png",
      name: "Komendant Główny Policji",
      phoneNr: "995"
    },
    {
      imgSrc: "/assets/ikonki/default.png",
      name: "Straż Miejska",
      phoneNr: "986"
    },
    {
      imgSrc: "/assets/ikonki/default.png",
      name: "Wodne Ochotnicze Pogotowie Ratunkowe",
      phoneNr: "985"
    },
    {
      imgSrc: "/assets/ikonki/default.png",
      name: "Tatrzańskie Ochotnicze Pogotowie Ratunkowe",
      phoneNr: "985"
    }
  ];

  constructor() {}

  ngOnInit() {}
}
