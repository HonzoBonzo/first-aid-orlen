import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmergencyNumbersViewComponent } from './emergency-numbers-view.component';

describe('EmergencyNumbersViewComponent', () => {
  let component: EmergencyNumbersViewComponent;
  let fixture: ComponentFixture<EmergencyNumbersViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmergencyNumbersViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmergencyNumbersViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
