import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-insurance-view",
  templateUrl: "./insurance-view.component.html",
  styleUrls: ["./insurance-view.component.scss"]
})
export class InsuranceViewComponent implements OnInit {
  sections = [
    {
      title: "Jak prawidłowo uzupełnić oświadczenie o zdarzeniu drogowowym",
      name: "W przypadku braku gotowego oświadczenia",
      image: "http://ferfecki.top/images/ubezp-0.jpg",
      description:
        "Kolejne kroki poprowadzą cię przez spisywanie oświadczenia aby zawierało wszystkie niezbędne dane"
    },
    {
      title: "Krok 1",
      name:
        "W razie ubiegania się o odszkodowanie po wypadku lub stłuczce, zakład ubezpieczeń będzie wymagał od nas:<br/>",
      image: "http://ferfecki.top/images/ubezp-01.jpg",
      description:
        "danych dotyczących pojazdu,<br/>danych dotyczących kierującego pojazdem,<br/>numeru polisy,<br/>nazwy i adresu zakładu ubezpieczeń, z którym właściciel pojazdu ma podpisaną umowę ubezpieczenia."
    },
    {
      title: "Krok 2",
      name:
        "Jakie informacje musi zawierać oświadczenie o okolicznościach zdarzenia?",
      image: "http://ferfecki.top/images/ubezp-01.jpg",
      description:
        "dokładny opis miejsca zdarzenia (np. ulica i nr domu, skrzyżowanie),<br/>dane identyfikujące auta (numery rejestracyjne),<br/>dokładne dane kierowcy (imię, nazwisko, adres, PESEL, kategoria prawa jazdy ),<br/>data, numer i organ wydający prawo jazdy,<br/>numer polisy i nazwę ubezpieczyciela,<br/>opis uszkodzeń samochodu/ów,<br/>wskazanie w oświadczeniu, kto ponosi winę za zdarzenie (wskazanie winnego wypadku nie zawsze może być zgodne z rzeczywistym stanem. Niekiedy nie wszystkie okoliczności wypadku są oczywiste, dlatego w razie jakichkolwiek niejasności warto jest wezwać policję)*<br/>szkic sytuacyjny, z zaznaczonymi pasami ruchu, znakami drogowymi, ustawieniem pojazdów przed i po kolizji (uwaga - niedokładny szkic może zostać wykorzystany przez ubezpieczyciela jako dowód stwierdzający, że „sprawca” jednak nie jest winny),<br/>o zdarzeniu natychmiast trzeba zawiadomić telefonicznie ubezpieczyciela (wymagają tego niektóre firmy),<br/>warto sporządzić zdjęcia z miejsca kolizji oraz spisać dane świadków (pomoże to w ewentualnym sporze z ubezpieczycielem)."
    },
    {
      title: "Krok 3",
      name: "Wezwanie policji w razie sporu:",
      image: "http://ferfecki.top/images/ubezp-03.jpg",
      description:
        "jeśli kierowcy nie mogą dojść do porozumienia, kto zawinił, sprawca nie ma polisy OC lub jest pijany, należy wezwać policję,<br/>funkcjonariusze ustalą winnego zdarzenia i mają obowiązek ukarać go mandatem. Uwaga ! Rozstrzygnięcie policji nie jest wiążące dla ubezpieczyciela, to tylko kolejny dowód w sprawie,<br/>funkcjonariusze sporządzą notatkę służbową, w której znajdą się wszystkie dane kierujących, biorących udział w kolizji. Na prośbę poszkodowanego powinni mu przekazać dane sprawcy,<br/> jeżeli sprawca nie zgadza się z oceną policjantów, ci muszą skierować wniosek rozstrzygnięcie sporu do Sądu Grodzkiego."
    },
    {
      title: "Krok 4",
      name: "Co dalej?",
      image: "http://ferfecki.top/images/ubezp-01.jpg",
      description:
        "W razie wypadku nie wolno nam dokonywać jakichkolwiek zmian w pojeździe. Jedynym wyjątkiem może być konieczność dokonania zmian w celu kontynuowania dalszej bezpiecznej jazdy. Nie można również dokonywać naprawy, ani korzystać z pomocy niezależnego rzeczoznawcy, jeśli wcześniej nie zostały przeprowadzone oględziny pojazdu przez przedstawiciela zakładu ubezpieczeń.<br/><br/>Po zdarzeniu drogowym powinniśmy jak najszybciej powiadomić naszego ubezpieczyciela, jednak nie może nastąpić to później niż w terminie określonym w ogólnych warunkach ubezpieczeń (zwykle 5-7 dni)."
    },
    {
      title: "Krok 5",
      name: "Co z odszkodowaniem",
      image: "http://ferfecki.top/images/ubezp-01.jpg",
      description:
        "Chcąc uzyskać odszkodowanie od zakładu ubezpieczeń musimy zastosować się do obowiązującej procedury. Przede wszystkim należy zgłosić szkodę na piśmie na obowiązującym druku zgłoszenia szkody. Zakład ubezpieczeń będzie wymagał od nas przedstawienia posiadanych dowodów dotyczących zaistnienia szkody, danych dotyczących uczestników zdarzenia i poniesionych kosztów. Aby ubezpieczyciel mógł ustalić rozmiary szkody należy ułatwić działanie wszelkim jego przedstawicielom lub służbom działającym na jego zlecenie. Musimy również umożliwić ubezpieczycielowi uzyskanie oświadczeń i niezbędnych informacji od osoby korzystającej z pojazdu podczas powstania szkody."
    },
    {
      title: "Krok 6",
      name: "",
      image: "",
      description: ""
    }
  ];

  constructor() {}

  ngOnInit() {}
}
