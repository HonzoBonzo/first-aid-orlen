import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-faq-view",
  templateUrl: "./faq-view.component.html",
  styleUrls: ["./faq-view.component.scss"]
})
export class FaqViewComponent implements OnInit {
  qas = [
    {
      q: "Czy mam obowiązek prawny udzielania pierwszej pomocy?",
      a:
        "Kodeks Karny nakłada obowiązek udzielenia pomocy wszystkim osobom w obliczu zagrożenia, jeśli nie naraża to osoby udzielającej pomocy na niebezpieczeństwo. Zatem każdy obywatel ma obowiązek udzielić pomocy w zakresie nieprzekraczającym własnych możliwości. Nieudzielenie pomocy jest karalne.\n    KK, Art. 162. § 1. Kto człowiekowi znajdującemu się w położeniu grożącym bezpośrednim niebezpieczeństwem utraty życia albo ciężkiego uszczerbku na zdrowiu nie udziela pomocy, mogąc jej udzielić bez narażenia siebie lub innej osoby na niebezpieczeństwo utraty życia albo ciężkiego uszczerbku na zdrowiu, podlega karze pozbawienia wolności do lat 3."
    },
    {
      q:
        "Czy gdy udzielając pierwszej pomocy „zrobię coś źle” mogę ponieść konsekwencje?",
      a:
        "Nie ma  podstaw do obaw o bezpieczeństwo prawne osoby ratującej w sytuacji pogorszenia się stanu poszkodowanego. Gdy osoba udzielająca pomocy wszystkie czynności wykonuje zgodnie ze swoją wiedzą i umiejętnościami, jest chroniona kolejny zapisem tego artykułu:\n    KK, Art. 162 § 2. Nie popełnia przestępstwa, kto nie udziela pomocy, do której jest konieczne poddanie się zabiegowi lekarskiemu albo w warunkach, w których możliwa jest niezwłoczna pomoc ze strony instytucji lub osoby do tego powołanej.\n    Można stąd wnioskować, iż wszelkie czynności w zakresie pierwszej pomocy nie są zabiegami specjalistycznymi. W warunkach, w jakich doszło do zdarzenia nie istnieje natychmiastowa możliwość uzyskania pomocy specjalisty. Zatem pierwsza pomoc jest maksymalnym zakresem, który można wdrożyć w chwili po zaistnieniu niebezpieczeństwa.\n    Osoba ratująca jest objęta ochroną prawną wg, Ustawy o Państwowym Ratownictwie Medycznym w oparciu o Kodeks Karny, mówiącym o ochronie przewidzianej dla osoby udzielającej pierwszej pomocy. Ochrana ta ujmuje tę osobę, jako funkcjonariusza publicznego.\n    Ust. o PRM, Art. 5. 1. Osoba udzielająca pierwszej pomocy, kwalifikowanej pierwszej pomocy oraz podejmująca medyczne czynności ratunkowe korzysta z ochrony przewidzianej w ustawie z dnia 6 czerwca 1997 r. - Kodeks karny (Dz. U. Nr 88, poz. 553, z późn. zm.2)) dla funkcjonariuszy publicznych."
    },
    {
      q:
        "Czy jeśli udzielając pierwszej pomocy zniszczę komuś odzież lub inne mienie poniosę jakieś konsekwencje?",
      a:
        "Osoba ratująca może wykorzystać mienie osób trzecich w celu ratowania życia lub zdrowia poszkodowanych\n    Ust. o PRM, Art. 5. 2. Osoba, o której mowa w ust. 1, może poświęcić dobra osobiste innej osoby, inne niż życie lub zdrowie, a także dobra majątkowe w zakresie, w jakim jest to niezbędne dla ratowania życia lub zdrowia osoby znajdującej się w stanie nagłego zagrożenia zdrowotnego\n    Przepisy te nie tylko chronią osobę ratującą przed złamaniem nietykalności cielesnej ale także stanowi ochronę prawną w przypadku poniesionej szkody na mieniu jak i w przypadku zdrowia"
    },
    {
      q: "Co oznacza skrót BLS?",
      a: "Basic Life Support – podstawowe zabiegi resuscytacyjne"
    },
    {
      q: "Co oznacza skrót AED?",
      a:
        "Skrót ten tłumaczy się na język polski, jako Automatyczny Elektryczny Defibrylator (AED).\n    W rzeczywistości, skrót AED zaczerpnięty został z języka angielskiego: automated external defibrillator i oznacza:\n    \n    • zautomatyzowany, czyli sterowany komputerowo, według zaprogramowanego algorytmu, który potrafi wykryć zagrażający życiu zbyt szybki rytm serca (migotanie komór serca – VF)\n    i poinformować ratownika sygnałem głosowym i świetlnym o konieczności defibrylacji .\n    \n    UWAGA: Po uruchomieniu AED urządzenie automatycznie instruuje i prowadzi ratownika przez cały algorytm reanimacji - nawet gdy nie trzeba defibrylować.\n    \n    • zewnętrzny, czyli działający za pomocą samoprzylepnych elektrod na skórze klatki piersiowej chorego.\n    \n    UWAGA: po przyklejeniu elektrod można je użyć u tego samego pacjenta kilkakrotnie (elektrody AED są „jednopacjentowe”, nie - jednorazowe).\n    \n    • defibrylator, a więc urządzenie wysyłające specjalnie opracowany impuls prądu stałego przez elektrody do serca, powodując tym samym powrót normalnego rytmu serca (60-90/min).\n    \n    UWAGA: Pamiętaj, by nie dotykać chorego po podłączeniu do AED!"
    },
    { q: "Co oznacza skrót RKO?", a: "Resuscytacja Krążeniowo – Oddechowa" }
  ];

  constructor() {}

  ngOnInit() {}
}
