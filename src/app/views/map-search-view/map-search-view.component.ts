import { Component, OnInit } from "@angular/core";
import {
  initialLayersWithTileLayer,
  defaultMapOptions
} from "src/app/services/osm/osm.config";
import { control, latLng, Map } from "leaflet";
import { ApiLocation } from "src/app/services/osm/osm.model";
import { createMarkerWithPopup } from "src/app/services/osm/osm.utils";
import { mockLocations } from "src/app/services/osm/locations.mock";

@Component({
  selector: "app-map-search-view",
  templateUrl: "./map-search-view.component.html",
  styleUrls: ["./map-search-view.component.scss"]
})
export class MapSearchViewComponent implements OnInit {
  map: Map;
  options = defaultMapOptions;
  layers = initialLayersWithTileLayer as any[];
  zoom = 15;
  locations: ApiLocation[] = [];

  constructor() {}

  ngOnInit(): void {
    this.layers = [...initialLayersWithTileLayer];
  }

  mapReady(map: Map): void {
    map.addControl(control.zoom({ position: "bottomright" }));
    this.map = map;
    this.map.invalidateSize();
    this.fetchAndSetMarkers();
    this.setViewToLocation();
  }

  fetchAndSetMarkers(): void {
    this.locations = mockLocations;
    this.locations.forEach(location => this.addMarker(location));
  }

  setViewToLocation(): void {
    const location = this.locations[0];
    const center = latLng(location.lat, location.lng);
    this.map.setView(center, 17);
  }

  addMarker(location: ApiLocation): void {
    this.layers = [
      ...this.layers,
      createMarkerWithPopup(location.lat, location.lng, location.description + ', adres: ' + location.address)
    ];
  }
}
