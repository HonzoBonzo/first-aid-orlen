import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapSearchViewComponent } from './map-search-view.component';

describe('MapSearchViewComponent', () => {
  let component: MapSearchViewComponent;
  let fixture: ComponentFixture<MapSearchViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapSearchViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapSearchViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
