import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeViewComponent } from "./views/home-view/home-view.component";
import { FirstAidViewComponent } from "./views/first-aid-view/first-aid-view.component";
import { QuizViewComponent } from "./views/quiz-view/quiz-view.component";
import { MapSearchViewComponent } from "./views/map-search-view/map-search-view.component";
import { AccidentViewComponent } from "./views/accident-view/accident-view.component";
import { ChecklistBeforeTripViewComponent } from "./views/checklist-before-trip-view/checklist-before-trip-view.component";
import { FaqViewComponent } from "./views/faq-view/faq-view.component";
import { InsuranceViewComponent } from "./views/insurance-view/insurance-view.component";
import { EmergencyNumbersViewComponent } from "./views/emergency-numbers-view/emergency-numbers-view.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "/home",
    pathMatch: "full"
  },
  {
    path: "home",
    component: HomeViewComponent
  },
  {
    path: "accident",
    component: AccidentViewComponent
  },
  {
    path: "first-aid",
    component: FirstAidViewComponent
  },
  {
    path: "map-search",
    component: MapSearchViewComponent
  },
  {
    path: "insurance",
    component: InsuranceViewComponent
  },
  {
    path: "quiz",
    component: QuizViewComponent
  },
  {
    path: "faq",
    component: FaqViewComponent
  },
  {
    path: "checklist-before-trip",
    component: ChecklistBeforeTripViewComponent
  },
  {
    path: "emergency-numbers",
    component: EmergencyNumbersViewComponent
  },
  {
    path: "**",
    redirectTo: "/home"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
